function clearExistingShots() {
  cy.visit("/shots");
  cy.contains("button", /clear/i).click();
}

function storeShotFor(userName) {
  cy.visit("/rekognition");
  cy.get("input").type(userName);
  cy.contains("button.enabled", /take/i).click();

  cy.contains("body", /saved/i, { timeout: 5000 });
}

function expectShotListLengthToEqual(value) {
  cy.visit("/shots");
  cy.get(".shot").then(userShots => {
    expect(userShots.length).to.eq(value);
  });
}

context("User shots", () => {
  it("store user shots", () => {
    const userName = "Ricardo";

    clearExistingShots();
    storeShotFor(userName);
    expectShotListLengthToEqual(1);
  });
});
