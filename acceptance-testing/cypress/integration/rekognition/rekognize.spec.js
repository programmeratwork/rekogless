function storeShotFor(userName) {
  cy.visit("/rekognition");
  cy.get("input").type(userName);
  cy.contains("button.enabled", /take/i).click();

  cy.contains("body", /saved/i, { timeout: 5000 });
}

function expectUserCanBeRekognized() {
  cy.visit("/rekognition");

  cy.contains("button.enabled", /guess/i).click();
  cy.contains("body", /hi /i, { timeout: 5000 });
}

context("User rekognition", () => {
  it("user can be rekognized when have an existing shot", () => {
    const userName = "Ricardo";

    storeShotFor(userName);
    expectUserCanBeRekognized(userName);
  });
});
