function expectPageTitleToEqual(title) {
  cy.visit("/");
  cy.title().should("include", title);
}

context("Branding", () => {
  it("show application name in the page title", () => {
    expectPageTitleToEqual("Rekogless");
  });
});
