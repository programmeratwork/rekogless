class User {
  constructor(name) {
    this.name = name;
  }

  toDto() {
    return { name: this.name };
  }
}

module.exports = User;
