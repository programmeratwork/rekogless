const AWS = require("aws-sdk");
const uuidv4 = require("uuid/v4");

const TABLE_NAME = "PEOPLE_SHOTS";

const client = new AWS.DynamoDB.DocumentClient();

async function clear() {
  const records = await findAll();

  await Promise.all(
    records.map(
      async ({ id }) =>
        await client
          .delete({
            TableName: TABLE_NAME,
            Key: {
              id
            }
          })
          .promise()
    )
  );
}

async function put(fileName, faceId, userName) {
  const uuid = uuidv4();

  await client
    .put({
      TableName: TABLE_NAME,
      Item: {
        id: uuid,
        fileName,
        faceId,
        userName
      }
    })
    .promise();
}

async function findAll() {
  const { Items } = await client
    .scan({
      TableName: TABLE_NAME
    })
    .promise();

  return Items;
}

async function findById(id) {
  const { Items } = await client
    .scan({
      TableName: TABLE_NAME,
      FilterExpression: "faceId = :id",
      ExpressionAttributeValues: { ":id": id }
    })
    .promise();

  return Items[0];
}

async function findByFileName(fileName) {
  const { Items } = await client
    .scan({
      TableName: TABLE_NAME,
      FilterExpression: "fileName = :fileName",
      ExpressionAttributeValues: { ":fileName": fileName }
    })
    .promise();

  return Items[0];
}

module.exports = {
  clear,
  put,
  findById,
  findByFileName,
  findAll
};
