const AWS = require("aws-sdk");

const s3 = new AWS.S3();

const REKOGLESS_BUCKET = "rekogless";

async function putImage(uuid, userName, image) {
  const fileName = `${uuid}.jpg`;

  await s3
    .putObject({
      Bucket: REKOGLESS_BUCKET,
      Key: fileName,
      Body: new Buffer(image, "base64"),
      ContentEncoding: "base64",
      ContentType: "image/jpeg",
      Metadata: {
        username: userName
      }
    })
    .promise();

  return fileName;
}

async function retrieveBucketImages() {
  const objects = await s3
    .listObjects({
      Bucket: REKOGLESS_BUCKET
    })
    .promise();

  const objectKeys = objects.Contents.map(object => object.Key);

  const dataObjects = await Promise.all(
    objectKeys.map(
      async Key =>
        await s3
          .getObject({
            Bucket: REKOGLESS_BUCKET,
            Key
          })
          .promise()
    )
  );

  return dataObjects.map(object => {
    return {
      username: object.Metadata.username,
      content: object.Body.toString("base64")
    };
  });
}

async function clearBucketImages() {
  const objects = await s3
    .listObjects({
      Bucket: REKOGLESS_BUCKET
    })
    .promise();

  const objectKeys = objects.Contents.map(object => object.Key);

  await Promise.all(
    objectKeys.map(
      async Key =>
        await s3
          .deleteObject({
            Bucket: REKOGLESS_BUCKET,
            Key
          })
          .promise()
    )
  );
}

module.exports = {
  putImage,
  retrieveBucketImages,
  clearBucketImages
};
