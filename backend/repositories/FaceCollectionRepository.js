const atob = require("atob");
const AWS = require("aws-sdk");

const { put, findById, clear } = require("./AuditRepository");

const REKOGLESS_BUCKET = "rekogless";

AWS.config = {
  rekognition: "2016-06-27"
};

const rekognition = new AWS.Rekognition();

async function recreateCollection() {
  try {
    await rekognition
      .deleteCollection({
        CollectionId: REKOGLESS_BUCKET
      })
      .promise();
  } catch (e) {
    console.log(`Collection ${REKOGLESS_BUCKET} does not exist`);
  }

  await rekognition
    .createCollection({
      CollectionId: REKOGLESS_BUCKET
    })
    .promise();

  await clear();
}

async function indexFace(fileName, userName) {
  const { FaceRecords } = await rekognition
    .indexFaces({
      CollectionId: REKOGLESS_BUCKET,
      Image: {
        S3Object: {
          Bucket: REKOGLESS_BUCKET,
          Name: fileName
        }
      }
    })
    .promise();

  const faceId = FaceRecords[0].Face.FaceId;

  await put(fileName, faceId, userName);

  return faceId;
}

async function findUserNameByImage(screenshot) {
  const imageBytes = base64ToBlob(screenshot);

  try {
    const result = await rekognition
      .searchFacesByImage({
        CollectionId: REKOGLESS_BUCKET,
        FaceMatchThreshold: 95,
        Image: {
          Bytes: imageBytes
        }
      })
      .promise();

    const { userName } = await findById(result.FaceMatches[0].Face.FaceId);
    return userName;
  } catch (e) {
    return undefined;
  }
}

function base64ToBlob(base64) {
  const image = atob(base64);
  let imageBytes = new ArrayBuffer(image.length);

  let uintArray = new Uint8Array(imageBytes);

  for (let i = 0; i < image.length; i++) {
    uintArray[i] = image.charCodeAt(i);
  }

  return imageBytes;
}

module.exports = {
  findUserNameByImage,
  recreateCollection,
  indexFace
};
