const {
  recreateCollection,
  indexFace
} = require("../repositories/FaceCollectionRepository");

const {
  putImage,
  retrieveBucketImages,
  clearBucketImages
} = require("../repositories/ImageStorageRepository");

const uuidv4 = require("uuid/v4");

async function retrieveShots() {
  return await retrieveBucketImages();
}

async function storeShot(userName, screenshot) {
  const uuid = uuidv4();

  const fileName = await putImage(uuid, userName, screenshot);
  await indexFace(fileName, userName);
}

async function clearShots() {
  await clearBucketImages();
  await recreateCollection();
}

module.exports = {
  retrieveShots,
  storeShot,
  clearShots
};
