const {
  findUserNameByImage
} = require("../repositories/FaceCollectionRepository");

const User = require("../domain/User");

async function rekognize(screenshot) {
  const userName = await findUserNameByImage(screenshot);

  if (!userName) return new User("Unkown");

  return new User(userName);
}

module.exports = {
  rekognize
};
