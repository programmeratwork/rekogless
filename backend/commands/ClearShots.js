const { clearShots } = require("../services/ShotsStorage");

module.exports.run = async event => {
  await clearShots();

  return {
    statusCode: 200
  };
};
