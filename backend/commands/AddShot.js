const { storeShot } = require("../services/ShotsStorage");

module.exports.run = async event => {
  const body = JSON.parse(event.body);
  const userName = body.userName;
  const screenshot = body.screenshot.split("data:image/jpeg;base64,")[1];

  await storeShot(userName, screenshot);

  return {
    statusCode: 200
  };
};
