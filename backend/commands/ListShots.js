const { retrieveShots } = require("../services/ShotsStorage");

module.exports.run = async event => {
  const objects = await retrieveShots();

  return {
    statusCode: 200,
    body: JSON.stringify(objects)
  };
};
