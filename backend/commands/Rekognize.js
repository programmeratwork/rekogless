const { rekognize } = require("../services/FaceDetection");

module.exports.run = async event => {
  const body = JSON.parse(event.body);
  const screenshot = body.screenshot.split("data:image/jpeg;base64,")[1];

  const user = await rekognize(screenshot);

  return {
    statusCode: 200,
    body: JSON.stringify(user.toDto())
  };
};
