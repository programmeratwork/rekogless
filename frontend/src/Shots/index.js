import React, { useState, useEffect } from "react";

import { retrieveShots, clearShots } from "./repositories/ShotsRepository";

export const Shots = () => {
  const [shots, setShots] = useState([]);

  async function load() {
    const { data: shots } = await retrieveShots();
    setShots(shots);
  }

  async function clear() {
    await clearShots();
    setShots([]);
  }

  useEffect(() => {
    load();
  }, []);

  return (
    <>
      <h1>Shots</h1>

      <p>Full list of all the screenshots that has been taken so far:</p>

      <div>
        <button onClick={load}>Reload list</button>
        <button onClick={clear}>Clear all the screenshots</button>
      </div>

      {shots.length === 0 && (
        <div className="shotlist">No screenshots available :/</div>
      )}

      <div className="shotlist">
        {shots.map(({ username, content }, index) => (
          <div key={index} className="shot">
            <img
              alt={`Rekognition shot for ${username}`}
              src={`data:text/plain;base64,${content}`}
              width="200"
            />
            <div>{username}</div>
          </div>
        ))}
      </div>
    </>
  );
};
