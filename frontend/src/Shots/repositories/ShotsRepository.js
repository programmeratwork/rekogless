import { dispatch } from "../../Core/CommandDispatcher";

export const storeShot = async (userName, screenshot) => {
  return await dispatch({
    type: "AddShot",
    userName,
    screenshot
  });
};

export const retrieveShots = async () => {
  return await dispatch({
    type: "ListShots"
  });
};

export const clearShots = async () => {
  return await dispatch({
    type: "ClearShots"
  });
};
