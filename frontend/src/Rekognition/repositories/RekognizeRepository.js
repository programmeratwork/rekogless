import { dispatch } from "../../Core/CommandDispatcher";

export const rekognizeUser = async screenshot => {
  return await dispatch({
    type: "Rekognize",
    screenshot
  });
};
