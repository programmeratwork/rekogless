import React, { useState, useEffect, useRef } from "react";
import Webcam from "react-webcam";

import { rekognizeUser } from "./repositories/RekognizeRepository";
import { storeShot } from "../Shots/repositories/ShotsRepository";

export const Rekognition = () => {
  let screenShotInput = useRef(null);
  let webcamEl = useRef(null);

  let [user, setUser] = useState(null);
  let [ready, setReady] = useState(false);
  let [userName, setUserName] = useState("");
  let [shotSaved, setShotSaved] = useState(false);

  async function rekognize() {
    const screenshot = webcamEl.current.getScreenshot();
    const { data } = await rekognizeUser(screenshot);

    setUser(data.name);
    setTimeout(() => setUser(null), 3000);
  }

  async function storeExample() {
    const screenshot = webcamEl.current.getScreenshot();

    await storeShot(userName, screenshot);

    setShotSaved(true);
    setTimeout(() => setShotSaved(false), 3000);
  }

  function handleExampleUserChange({ target: { value } }) {
    setUserName(value);
  }

  useEffect(() => {
    screenShotInput.current.focus();
  }, []);

  const userMediaReadyClass = ready ? "enabled" : "disabled";

  return (
    <>
      <h1>Rekognition</h1>

      <p>
        Add new shots to Rekogless catalog or try to guess who is in front of
        the cam:
      </p>

      <div>
        <input
          className="takeshot-input"
          placeholder="enter user name"
          ref={screenShotInput}
          onChange={handleExampleUserChange}
          value={userName}
        />

        <button
          className={`takeshot-button ${userMediaReadyClass}`}
          disabled={!ready}
          onClick={storeExample}
        >
          Take a shot!!
        </button>

        {shotSaved && (
          <span className="feedback-message" style={{ marginLeft: "1rem" }}>
            Shot saved :)
          </span>
        )}
      </div>

      <div style={{ marginTop: "1rem", marginBottom: "1rem" }}>
        <Webcam
          ref={webcamEl}
          audio={false}
          screenshotFormat="image/jpeg"
          onUserMedia={() => setReady(true)}
        />
      </div>

      <div>
        <button
          className={`guess-button ${userMediaReadyClass}`}
          disabled={!ready}
          onClick={rekognize}
        >
          Guess who am I ...
        </button>

        {user && <span className="feedback-message">Hi {user}!!</span>}
      </div>
    </>
  );
};
