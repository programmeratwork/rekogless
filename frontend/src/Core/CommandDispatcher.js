import axios from "axios";

export async function dispatch(command) {
  const { type } = command;
  const { data } = await axios.post(`/${type}/${type}`, command);

  return { success: true, data };
}
