import React from "react";
import { Switch, Route, Link } from "react-router-dom";

import { Rekognition } from "./Rekognition";
import { Shots } from "./Shots";

export const App = () => (
  <div>
    <aside>
      <div className="logo">
        <img alt="Logo" src="/images/lambda.png" />
        <h3>Rekogless</h3>
      </div>

      <ul>
        <li>
          <Link to="/rekognition">Rekognition</Link>
        </li>
        <li>
          <Link to="/shots">Shots</Link>
        </li>
      </ul>
    </aside>

    <section>
      <Switch>
        <Route path="/rekognition" component={Rekognition} />
        <Route path="/shots" component={Shots} />
      </Switch>
    </section>
  </div>
);
